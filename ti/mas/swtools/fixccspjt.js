
function usage()
 {
     print("\n\tUSAGE:\n");
     print("\t\txs -f fixccspjt.js input\n");
     java.lang.System.exit(1);
 }



function fixcdt(cdt)
{
     var srcFile=String(cdt);
     var dot = srcFile.lastIndexOf(".");
     var extension = srcFile.substr(dot,srcFile.length);
     /* Check for the right input */
     if (extension == ".cdtbuild" || extension == ".cproject"  )
     {
       var fileModule = xdc.module('xdc.services.io.File');
       var openedFile= fileModule.open(srcFile,"r");
       if(openedFile == null)
       {
          print("Error: Cannot open " +srcFile+" file ");
          usage();
       }
       /* Temporary file to write  */
       var tmpfile = fileModule.open("temp.xml","w");
       var readLine;
       while((readLine=openedFile.readLine()) != null)
        {
            
            if(readLine.match(/[$]{Path}/))
            {
              readLine=readLine.replace(/[$]{Path}/g,"&quot;${Path}&quot;");
            }
            else if(readLine.match('listOptionValue builtIn="false"'))
            {

              var bracket = readLine.indexOf("{");
              var dollar = readLine.indexOf("$");
              var equal = readLine.lastIndexOf("=");
              if(bracket != -1 && equal != -1)
              {
	          var isdollar = readLine.slice(equal+2,bracket);
	          if(dollar == -1 && isdollar != "$")
        	  {
	            readLine = readLine.replace(isdollar,"$");
              	  }
              }
            }
            tmpfile.writeLine(readLine);
        }  
       openedFile.close();
       tmpfile.close();

       /* Replace source file with Temp file */
       /* Copy Module */
       var copy = xdc.loadCapsule('ti/mas/swtools/Copy.xs');
       copy.Move("temp.xml",cdt);
     }
     else
     {
       print(cdt +"is not a valid ccs project file"); 
       java.lang.System.exit(1); 
     }
 

}

/* Main function starts here */

if(arguments.length < 1)
{
  usage();
}

fixcdt(arguments[0]);
