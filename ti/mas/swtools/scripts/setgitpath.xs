
var fileModule = xdc.module('xdc.services.io.File');
var fileName   = "tempcfg.bat";
var Mode       = "w";
var openedFile = fileModule.open(fileName, Mode);

if (openedFile == undefined) {
  print ("Unable to create "+ fileName+", Please check your write Permission");
  java.lang.System.exit(2);
}  

/* Set GIT path */
var pathFile = fileModule.open("path.txt", "r");
var paths=pathFile.readLine();
paths=paths.toString().split(";");
var gitPath="";
for each (var path in paths)
{
  if(path.toString().toLowerCase().match("git"))
  {
    path=path.toString().replace(/\r/g,"");
	path=path.toString().replace(/\n/g,"");
	gitPath=path;
    break;
   }

}
if (gitPath) {
  /* print path */

  var command="set PATH=%PATH%;"+ gitPath+";";
  openedFile.writeLine(command+"\n");
  openedFile.writeLine("");
}
