@echo off
:: *************************************************************************
::  FILE           : install_tools.bat
::  DESCRIPTION    :
::
::     This batch script installs all tools required to
::     build a SWTOOLS release.
::
:: *************************************************************************

@echo Executing:  %~fn0

set PATH=C:/tools;%PATH%

echo %PATH% > %~dp0/path.txt

:: *************************************************************************
:: *** Specify install locations
:: *************************************************************************
set SWTOOLS_WORKING_DIR=C:\SWTOOLS_Install
set SWTOOLS_INSTALL_DIR=C:/SWTOOLS_Tools
set SWTOOLS_INSTALL_DOS_DIR=C:\SWTOOLS_Tools

:: *************************************************************************
:: *** Specify tool versions
:: *************************************************************************
set XDC_VERSION=3_25_00_48
set CYG_VERSION=99-11-01
set DOX_VERSION=1.5.1-p1
set GVZ_VERSION=2.12
set HHW_VERSION=10-01-2007
set TIT_VERSION=10-01-2007



:: *************************************************************************
:: *** Specify install locations
:: *************************************************************************
set XDC_INSTALL_DIR=%SWTOOLS_INSTALL_DIR%/xdctools_%XDC_VERSION%
set CYG_INSTALL_DIR=%SWTOOLS_INSTALL_DIR%
set DOX_INSTALL_DIR=%SWTOOLS_INSTALL_DIR%
set DOX_INSTALL_DOS_DIR=%SWTOOLS_INSTALL_DOS_DIR%


:: *************************************************************************
:: *** Specify WGET locations
:: *************************************************************************
::  set XDC_WGET_URL="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/rtsc/%XDC_VERSION%/exports"
set XDC_WGET_URL="http://www.sanb.design.ti.com/tisb_releases/XDCtools/%XDC_VERSION%/exports"
set CYG_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/cygwin"
set DOX_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/doxygen"


:: *************************************************************************
:: *** Specify WGET packages
:: *************************************************************************
set XDC_WGET_PKG=xdctools_setupwin32_%XDC_VERSION%.exe
set CYG_WGET_PKG=cygwin-%CYG_VERSION%.zip
set DOX_WGET_PKG=doxygen.zip

:: *************************************************************************
:: ** These are required for local build without tools support 
:: *************************************************************************
set TI_DOXYGEN_TEMPLATES=%DOX_INSTALL_DIR%/Doxygen/TI_Templates/%TIT_VERSION%

:: *************************************************************************
:: *************************************************************************
:: ** Start installation process
:: *************************************************************************
:: *************************************************************************
@echo .
@echo #######################################################################
@echo ##  Installing Required Tools
@echo #######################################################################
@echo .

:: *************************************************************************
:: *** Create installation directories
:: *************************************************************************
if not exist "%SWTOOLS_WORKING_DIR%" mkdir "%SWTOOLS_WORKING_DIR%"
@cd %SWTOOLS_WORKING_DIR%

if not exist "%SWTOOLS_INSTALL_DIR%" mkdir "%SWTOOLS_INSTALL_DIR%"


:: *************************************************************************
:: ** Install XDC
:: *************************************************************************
if exist %XDC_WGET_PKG% goto xdc_install
@echo WGET:  %XDC_WGET_PKG%
wget -nc --no-proxy %XDC_WGET_URL%/%XDC_WGET_PKG%
if not exist %XDC_WGET_PKG% goto xdc_error
:xdc_install
@echo Install:  %XDC_WGET_PKG%
if exist %XDC_INSTALL_DIR% goto xdc_exist
%XDC_WGET_PKG% --prefix %XDC_INSTALL_DIR% --mode silent
:xdc_exist
@echo ......... %XDC_WGET_PKG%  Installed
@echo .

:: *************************************************************************
:: ** Set the PATH
:: *************************************************************************
set PATH=%XDC_INSTALL_DIR%;%PATH%
set PATH=%XDC_INSTALL_DIR%/bin;%PATH%


:: *************************************************************************
:: ** Install Cygwin
:: *************************************************************************
if exist %CYG_WGET_PKG% goto cyg_install
@echo WGET:  %CYG_WGET_PKG%
wget -nc --no-proxy %CYG_WGET_URL%/%CYG_WGET_PKG%
if not exist %CYG_WGET_PKG% goto cyg_error
:cyg_install
@echo Install:  %CYG_WGET_PKG%
if exist %CYG_INSTALL_DIR%/cygwin goto cyg_exist
@unzip -o -d %CYG_INSTALL_DIR% %CYG_WGET_PKG%
:cyg_exist
@echo ......... %CYG_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install Doxygen
:: *************************************************************************
if exist %DOX_WGET_PKG% goto dox_install
@echo WGET:  %DOX_WGET_PKG%
wget -nc --no-proxy %DOX_WGET_URL%/%DOX_WGET_PKG%
if not exist %DOX_WGET_PKG% goto dox_error
:dox_install
@echo Install:  %DOX_WGET_PKG%
if exist %DOX_INSTALL_DIR%/Doxygen goto dox_exist
@unzip -o -d %DOX_INSTALL_DIR% %DOX_WGET_PKG%
:dox_exist
@echo ......... %DOX_WGET_PKG%  Installed
@echo .


@echo #######################################################################
@echo ##  All Required Tools Installed
@echo #######################################################################
@echo .



:: *************************************************************************
:: ** Set the PATH
:: *************************************************************************
set PATH=C:/tools;%SystemRoot%;%SystemRoot%/system32
set PATH=%XDC_INSTALL_DIR%;%PATH%
set PATH=%XDC_INSTALL_DIR%/jre/bin;%PATH%
set PATH=%CYG_INSTALL_DIR%/cygwin/bin;%PATH%
set PATH=%DOX_INSTALL_DOS_DIR%\Doxygen\doxygen\%DOX_VERSION%\bin;%PATH%
set PATH=%DOX_INSTALL_DOS_DIR%\Doxygen\Graphviz\%GVZ_VERSION%\bin;%PATH%
set PATH=%DOX_INSTALL_DOS_DIR%\Doxygen\HTML_Help_Workshop\%HHW_VERSION%;%PATH%
set PATH=%TI_DOXYGEN_TEMPLATES%;%PATH%

:: *************************************************************************
:: ** Create XDC environment variables
:: *************************************************************************
@cd %~dp0

set xdc=%XDC_INSTALL_DIR%\xdc.exe $*
set XDCPATH=%XDCPATH%;%XDC_INSTALL_DIR%/packages

xs -f setgitpath.xs
if errorlevel 1 goto clean
call tempcfg
:clean
rm -rf tempcfg.bat
rm -rf path.txt

:: *************************************************************************
:: ** Create environment variables for XDC
:: *************************************************************************
@cd %~dp0
:: *************************************************************************
:: ** Show the build environment
:: *************************************************************************
@echo .
@echo #######################################################################
@echo ##  Build Environment Variables (Start)
@echo #######################################################################
@set
@echo #######################################################################
@echo ##  Build Environment Variables (Stop)
@echo #######################################################################
@echo .


:: *************************************************************************
:: ** Errors and cleanup
:: *************************************************************************
goto install_end

:xdc_error
@echo Unable to find XDC installation:  %XDC_WGET_PKG%  exiting...
goto install_end


:cyg_error
@echo Unable to find Cygwin installation:  %CYG_WGET_PKG%  exiting...
goto install_end

:dox_error
@echo Unable to find Doxygen installation:  %DOX_WGET_PKG%  exiting...
goto install_end


:: *************************************************************************
:: *** Cleanup and return
:: *************************************************************************
:install_end
cd ..

set XDC_WGET_URL=
set CYG_WGET_URL=
set DOX_WGET_URL=

set CYG_VERSION=
set DOX_VERSION=
set GVZ_VERSION=
set HHW_VERSION=
set TIT_VERSION=


set XDC_WGET_PKG=
set CYG_WGET_PKG=
set DOX_WGET_PKG=


:end
:: *************************************************************************
:: *** Nothing past this point
:: *************************************************************************
