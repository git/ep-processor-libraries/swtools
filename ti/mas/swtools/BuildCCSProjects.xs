function usage()
 {
     print("\n\tUSAGE:\n");
     print("\t\txs -f BuildCCSProjects.xs srcDir batch [options]\n");
     java.lang.System.exit(1);
 }

 function parseArgs(arguments)
 {
    inputDir = arguments[0];
	OutputFile = arguments[1];
	if(arguments[2] != undefined)
	{
      config = arguments[2];
	}
	else
	{
	  config = "Release";
	}
    print("Input Directory:"+inputDir);
	print("Output File:"+OutputFile);
    print("Configuration:"+config);
 }

function searchFiles(dirs,recurse)
{
  var file = new java.io.File(dirs);
  if(!file.isDirectory())
  {
    print("Error: "+dirs+" is not a directory");
    usage();
  }

  var list = file.list();
  var i, j;
  for (i = 0; i < list.length; i++)
  {
     if(recurse == true && java.io.File(dirs+"/"+list[i]).isDirectory())
     {
        searchFiles(dirs+"/"+list[i],true);
     }
     var srcFile=String(list[i]);
     var dot = srcFile.lastIndexOf(".");
     var extension = srcFile.substr(dot,srcFile.length);      
     if (extension == ".project")
     {
        srcArray.push(dirs);
     }
      
  }
}



/* Main Function starts here */

var env   = java.lang.System.getenv();
    var keys  = env.keySet().toArray();
    var key;
    var stat={};
    var env_j=[];
    for (var i = 0; i < keys.length; i++) {
         key = keys[i];
      if (env.get(key) == "") {
             continue;
         }
         env_j = env_j.concat([key + '=' + env.get(key)]);
     }
   var attrs = {
        envs: env_j
   }

var inputDir;
var outputFile;
var config;
var readLine;
var outputFileString = "";
var srcArray = new Array();
var recurse=false;

/* Check for valid number of Arguments */
if(arguments.length < 2)
{
   usage();
}


/* parse Arguments */
parseArgs(arguments);

/* Check for valid Arguments */
if(inputDir == undefined)
{
   java.lang.System.exit(1);
}

/*Open outputFile file and copy the string to string bugger */
var fileModule = xdc.module('xdc.services.io.File');
var batch = fileModule.open(OutputFile,"w");
/* Copy Module */
var copy = xdc.loadCapsule('ti/mas/swtools/Copy.xs');

/* Search for out files */
searchFiles(inputDir,true);

batch.writeLine("@echo off\n");



for each(var file in srcArray)
{
  var pjtname = String(file);
  pjtname = pjtname.replace(/\\/g, '/');
  pjtname = pjtname.split("/");
  pjtname = pjtname[pjtname.length -1];
  batch.writeLine("rm -rf c:\\Temp\\workspace\\"+pjtname);
  batch.writeLine("@echo Importing "+ pjtname + " ...");
  batch.writeLine("\"%CCSV5_INSTALL_DIR%/eclipse/eclipsec.exe\" -noSplash -data c:\\Temp\\workspace\\"+pjtname+" -application com.ti.ccstudio.apps.projectImport  -ccs.location "+ file+ "\n");
  batch.writeLine("@echo Building "+ pjtname + " ...");
  batch.writeLine("\"%CCSV5_INSTALL_DIR%/eclipse/eclipsec.exe\" -noSplash -data c:\\Temp\\workspace\\"+pjtname+" -application com.ti.ccstudio.apps.projectBuild  -ccs.projects "+ pjtname+ " -ccs.configuration "+ config+"\n");
  batch.writeLine("@echo Finished "+ pjtname + " ...");
  batch.writeLine("rm -rf c:\\Temp\\workspace\\"+pjtname);
  batch.writeLine("\n");
}
 
 batch.close(); 



 

